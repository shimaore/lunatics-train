use lunatic::{process, Mailbox};

#[lunatic::main]
fn main(m: Mailbox<()>) {
    // Get handle to itself.
    let this = process::this(&m);
    process::spawn_with(this, |parent, _: Mailbox<()>| {
        // This closure gets a new heap and stack to
        // execute on, and can't access the memory of
        // the parent process.
        println!("Hi! I'm a process.");
        parent.send(());
    })
    .unwrap();
    // Wait for child to finish.
    let _ = m.receive();
}
