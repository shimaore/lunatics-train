// To run this example you will need to have a remote node under the name "foo".
// You can start one with: `lunatic --node 0.0.0.0:8333 --node-name foo --no-entry`
//
// This example also requires some command line arguments to the runner:
// > cargo build --example environment_remote
// > lunatic --node 0.0.0.0:8334 --node-name bar --peer 0.0.0.0:8333 target/wasm32-wasi/debug/examples/environment_remote.wasm

use lunatic::{Config, Environment, Mailbox, Request};

#[lunatic::main]
fn main(_: Mailbox<i64>) {
    let mut config = Config::new(0xA00000000, None);
    config.allow_namespace("");
    let mut env = Environment::new_remote("foo", config).unwrap();
    let module = env.add_this_module().unwrap();

    // Spawn child
    let child = module
        .spawn(|mailbox: Mailbox<Request<(i64, i64), i64>>| loop {
            let request = mailbox.receive().unwrap();
            let (a, b) = *request.data();
            println!("Adding {} + {}", a, b);
            request.reply(a + b);
        })
        .unwrap();

    let result = child.request((23,4)).unwrap();
    println!("Adding {} + {} = {}", 23, 4, result);

    let result = child.request((27,9)).unwrap();
    println!("Adding {} + {} = {}", 27, 9, result);

    // FIXME: how to stop things?
}
