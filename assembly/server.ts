import * as lunatic from "as-lunatic";

const TCP_PORT : u16 = 3030;

function stringToBuffer(s: string) : StaticArray<u8> {
  const len = String.UTF8.byteLength(s);
  const result = new StaticArray<u8>(len);
  const buf = Uint8Array.wrap(String.UTF8.encode(s));
  for( let i = 0; i < buf.length; i++ ) {
    result[i] = buf.at(i);
  }
  return result;
}

function processSocket(socket: lunatic.TCPStream): void {
  // do something with the accepted tcp socket here
  const body : StaticArray<u8> = stringToBuffer(
    '<html><body>Welcome world!</body></html>'
  );
  const headers: StaticArray<u8> = stringToBuffer(
    'HTTP/1.1 200 OK\r\n' +
    'Content-type: text/html\r\n' +
    `Content-length: ${body.length}\r\n`
  );
  socket.writeBuffer(headers);
  socket.writeBuffer(stringToBuffer('\r\n'));
  socket.writeBuffer(body);
  socket.flush();
}

function processServer(server: lunatic.TCPServer): void {
  let socketMaybe: lunatic.TCPResult<lunatic.TCPStream | null>;
  // console.log("[Pass] Accepting connections")

  // blocks until a socket is accepted
  while (socketMaybe = server.accept()) {
    if(!socketMaybe.value) {
      console.error('[Fail] Accepting connections')
      return
    }
    const socket : lunatic.TCPStream = socketMaybe.value!
    // pass the socket off to another process
    lunatic.Process.spawn<lunatic.TCPStream>(socket,processSocket)
    socket.drop()
  }
}

export function _start(): void {
  // console.log("[Pass] Starting")

  // bind the server to an ip address and a port
  let serverMaybe = lunatic.TCPServer.bind([0, 0, 0, 0], TCP_PORT);
  if (!serverMaybe.value) {
    console.error("[Fail] unable to bind");
    return;
  }
  const server = serverMaybe.value!
  lunatic.Process.spawn<lunatic.TCPServer>(server, processServer) // .detach();
}
